#!/usr/bin/python

'''
VMDK Sparse Extents Header Format (from Virtual Disk Format 5.0 by VMware)

#define COWDISK_MAX_PARENT_FILELEN 1024
#define COWDISK_MAX_NAME_LEN 60
#define COWDISK_MAX_DESC_LEN 512
typedef struct COWDisk_Header {
    uint32 magicNumber;
    uint32 version;
    uint32 flags;
    uint32 numSectors;
    uint32 grainSize;
    uint32 gdOffset;
    uint32 numGDEntries;
    uint32 freeSector;
    union {
        struct {
            uint32 cylinders;
            uint32 heads;
            uint32 sectors;
        } root;
        struct {
            char parentFileName[COWDISK_MAX_PARENT_FILELEN];
            uint32 parentGeneration;
        } child;
    } u;
    uint32 generation;
    char name[COWDISK_MAX_NAME_LEN];
    char description[COWDISK_MAX_DESC_LEN];
    uint32 savedGeneration;
    char reserved[8];
    uint32 uncleanShutdown;
    char padding[396];
} COWDisk_Header;

### Notes:
- magicNumber is set to 0x44574f43 which is ASCII COWD.
- version - The value of this entry should be 1.
- flags is set to 3.
- numSectors refers to total number of sectors on the base disk.
- grainSize is the granularity of data stored in delta links. This varies from 
  one sector (the default) to 1MB.
- gdOffset starts at the fourth sector, because the COWDisk_Header structure
  takes four sectors.
- numGDEntries is CEILING(numSectors, gtCoverage)
- freeSector is the next free data sector. It must be less than the length of
  the delta link. It is initially set to  gdOffset + numGDSectors;
- savedGeneration is used to detect the unclean shutdown of the delta link. It
  is initially set to 0.
- uncleanShutDown is used to trigger the metadata consistency check in case
  there is an abnormal termination of the program.
- The remaining fields are not used. They are present for compatibility with
  legacy virtual disk formats.

### ESXi Host Sparse Extent Metadata
The metadata for an ESXi host sparse extent is similar to that for a sparse
extent in a hosted VMware product, as described in  Hosted Sparse Extent
Metadata  on page 8, with the following exceptions:
- ESXi sparse extents do not include redundant copies of the grain directory.
- Grain tables have 4096 entries.
- Each grain contains 512 bytes.
'''


import struct
import sys
import os
import math

SPARSE_MAGICNUMBER = 0x44574f43
SPARSE_VERSION = 1
SPARSE_FLAGS = 3
GRAINS_PER_TABLE = 4096
SECTOR_SIZE = 512
#stats
G_WRITTEN = 0

def printHelp():
    print "Usage: %s <src> <dst>" % (sys.argv[0])


class COWDisk_Header(struct.Struct):
    format = '8I 3I 1024s I I 60s 512s I 8s I'
    def __init__(self):
        struct.Struct.__init__(self, self.format)

    def parse(self, bytes):
        (self.magicNumber,
         self.version,
         self.flags,
         self.numSectors,
         self.grainSize,
         self.gdOffset,
         self.numGDEntries,
         self.freeSector,
         self.cylinders,
         self.heads,
         self.sectors,
         self.parentFileName,
         self.parentGeneration,
         self.generation,
         self.name,
         self.description,
         self.savedGeneration,
         self.reserved,
         self.uncleanShutdown) = self.unpack(bytes)
        # calculate some useful numbers
        self.gTblCoverage = GRAINS_PER_TABLE * self.grainSize

    def printInfo(self):
        print "Header length: %d bytes" % (self.size)
        print "magicNumber: 0x%x" % (self.magicNumber)
        print "version: %s" % (self.version)
        print "flags: %s" % (self.flags)
        print "numSectors: %s" % (self.numSectors)
        print "grainSize: %s" % (self.grainSize)
        print "gdOffset: %s" % (self.gdOffset)
        print "numGDEntries: %s" % (self.numGDEntries)
        print "freeSector: %s" % (self.freeSector)
        print "cylinders: %s" % (self.cylinders)
        print "heads: %s" % (self.heads)
        print "sectors: %s" % (self.sectors)
        print "parentFileName: %s" % (self.parentFileName)
        print "parentGeneration: %s" % (self.parentGeneration)
        print "generation: %s" % (self.generation)
        print "name: %s" % (self.name)
        print "description: %s" % (self.description)
        print "savedGeneration: %s" % (self.savedGeneration)
        print "reserved: %s" % (self.reserved)
        print "uncleanShutdown: %s" % (self.uncleanShutdown)
        print ("\nCalculated values:\n "
               "Grain Table Coverage: %d (bytes)" % (self.gTblCoverage * SECTOR_SIZE) )


def mergeDisks(srcfile, dstfile):
    global G_WRITTEN

    try:
        sfd = os.open(srcfile, os.O_RDONLY)
        dfd = os.open(dstfile, os.O_RDWR,)
    except OSError as e:
        print "Error: %s: %s" % (e.filename, e.strerror)
        exit(1)

    hdr = COWDisk_Header()
    hdr.parse( os.read(sfd, hdr.size) )
    print "Header info:\n============"
    hdr.printInfo()
    print ""
    sys.stdout.flush()
    
    os.lseek(sfd, hdr.gdOffset * SECTOR_SIZE, os.SEEK_SET)
    numGTables = hdr.numGDEntries
    # each Grain Directory Entry is 4 bytes in size
    gTables = struct.unpack("<%dI" % (numGTables), os.read(sfd, numGTables * 4))

    if not sanityCheck(sfd, gTables, hdr):
        sys.stderr.write("Sanity check failed!\n")
        exit(1)
    else:
        sys.stdout.write("no errors found.\n\n")
 
    print "Merging data...\n==============="
    # iterate over Grain Directory Entries
    for i in range(numGTables):
        gTblOffset = gTables[i]
        if gTblOffset > 1:
            mergeGrainTable(sfd, dfd, i, gTblOffset, hdr)

    os.close(dfd)
    os.close(sfd)

    # print stats
    print "\nStats:\n======"
    print "Bytes written: %d" % (G_WRITTEN * hdr.grainSize * SECTOR_SIZE) 


def mergeGrainTable(sfd, dfd, gDirEntIdx, gTblOffset, hdr):
    global G_WRITTEN

    os.lseek(sfd, gTblOffset * SECTOR_SIZE, os.SEEK_SET)
    # each Grain Table Entry is 4 bytes in size
    gTbl = struct.unpack("<%dI" % (GRAINS_PER_TABLE),
                         os.read(sfd, GRAINS_PER_TABLE * 4) )
    i = 0
    while i < GRAINS_PER_TABLE:
        gOffset = gTbl[i]
        if gOffset > 1:
            os.lseek(sfd, gOffset * SECTOR_SIZE, os.SEEK_SET)
            gDataBuf = os.read(sfd, hdr.grainSize * SECTOR_SIZE)
            grainsRead = 1
            # read as many adjacent Grains as possible
            for k in range(i+1, GRAINS_PER_TABLE):
                gOffset = gTbl[k]
                if gOffset < 2:
                    # found a gap, quit reading and write out what you got
                    break
                os.lseek(sfd, gOffset * SECTOR_SIZE, os.SEEK_SET)
                gDataBuf += os.read(sfd, hdr.grainSize * SECTOR_SIZE)
                grainsRead += 1
        
            # write out here
            dpos = (gDirEntIdx * hdr.gTblCoverage + i * hdr.grainSize) * SECTOR_SIZE
            os.lseek(dfd, dpos, os.SEEK_SET)
            os.write(dfd, gDataBuf)
            # save some stats
            G_WRITTEN += grainsRead

            i += grainsRead
        else:
            i += 1


def sanityCheck(sfd, gTables, hdr):
    sys.stdout.write("Sanity check in progress... ")
    ret = 1
    fsize = os.fstat(sfd).st_size
    if hdr.magicNumber != SPARSE_MAGICNUMBER:
        sys.stderr.write("ERROR: Wrong magic number!\n")
        ret = 0
    if hdr.version != SPARSE_VERSION:
        sys.stderr.write("ERROR: Wrong version!\n")
        ret = 0
    if hdr.flags != SPARSE_FLAGS:
        sys.stderr.write("ERROR: Wrong flags!\n")
        ret = 0
    if hdr.numGDEntries != math.ceil(hdr.numSectors / float(GRAINS_PER_TABLE) ):
        sys.stderr.write("ERROR: numGDEntries does not match numSectors!\n")
        ret = 0
    if not hdr.freeSector * SECTOR_SIZE < fsize:
        sys.stderr.write("ERROR: freeSector is too far (must be less than file"
                         "size!\n")
        ret = 0
    sys.stdout.flush()

    for i in range(len(gTables)):
        gTblOffset = gTables[i]
        if gTblOffset > 1 :
            if gTblOffset * SECTOR_SIZE > fsize:
                print ("Warning: Grain Table offset is higher than source file "
                       "size, skipping GTE checks for this table. "
                       "GDE Index: %d" % (i) )
                ret = 0
                continue
            ret = checkGTbl(sfd, i, gTblOffset, hdr)
    sys.stdout.flush()
    return ret


def checkGTbl(sfd, gDirEntIdx, gTblOffset, hdr):
    fsize = os.fstat(sfd).st_size
    os.lseek(sfd, gTblOffset * SECTOR_SIZE, os.SEEK_SET)
    # each grain table entry is 4 bytes in size
    gTbl = struct.unpack("<%dI" % (GRAINS_PER_TABLE),
                         os.read(sfd, GRAINS_PER_TABLE * 4) )
    ret = 1
    for i in range(GRAINS_PER_TABLE):
        gOffset = gTbl[i]
        if gOffset * SECTOR_SIZE > fsize:
            print ("Warning: Grain offset value higher than source file size! "
                   "GDE Idx: %d, GTE Idx: %d, Grain offset: "
                   "%d (bytes)" % (gDirEntIdx, i, gOffset * SECTOR_SIZE) )
            ret = 0
    return ret
    

try:
    srcfile = sys.argv[1]
    dstfile = sys.argv[2]
except IndexError:
    printHelp()
    exit(1)

try:
    mergeDisks(srcfile, dstfile)
except KeyboardInterrupt:
    exit(2)

exit(0)
